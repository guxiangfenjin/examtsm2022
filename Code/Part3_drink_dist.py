import os
import csv
from matplotlib import pyplot as plt

COURSES = {0: 'First Course', 1: 'Second Course', 2: 'Third Course'}


def read_data(infile):
    with open(infile) as f:
        row = csv.reader(f, delimiter=',')
        header = next(row)  # read first line
        data = []
        for r in row:
            data.append(r)
    return data


def plot(costs):
    plt.figure(figsize=(16, 8))
    colors = ['r', 'g', 'b']
    for idx in range(3):
        plt.subplot(3, 1, idx + 1)
        plt.title(COURSES[idx])
        plt.hist(costs[idx], bins=200, color=colors[idx])
    plt.show()

if __name__ == '__main__':
    data = read_data(os.path.join('..', 'part1_food_drinks.csv'))
    costs = [[], [], []]
    idx = 0
    for items in data:
        if len(items)== 0:
            continue
        three_course_drink = [items[6], items[8], items[10]]
        for course_id, cost in enumerate(three_course_drink):
            if cost != '' and float(cost) != 0:
                costs[course_id].append(float(cost))
        idx += 1

    plot(costs)