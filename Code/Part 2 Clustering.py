import os
import csv

import numpy as np
from sklearn.cluster import KMeans

MENU = {'Soup': 3, 'Tomato-Mozarella': 15, 'Oystere': 20,
        'Salad':9, 'Spaghetti': 20, 'Steak':25, 'Lobster':40,
        'Ice cream': 15, 'Pie': 10}
INF = float('inf')

def read_data(infile):
    with open(infile) as f:
        row = csv.reader(f, delimiter = ',')
        header = next(row)  # read first line
        data = []
        for r in row:
            r[2:] = map(float, r[2:])
            data.append(r)
    return data


def plot(course1_costs, course2_costs, course3_costs):
    plt.figure(figsize=(18, 8))

    plt.subplot(311)
    plt.plot(course1_costs, color='blue')

    plt.subplot(312)
    plt.plot(course2_costs, color='green')

    plt.subplot(313)
    plt.plot(course3_costs, color='red')
    plt.show()


def barplot(course1_costs, course2_costs, course3_costs):
    plt.figure(figsize=(18, 8))

    plt.subplot(311)
    sns.barplot(course1_costs, color='blue')

    plt.subplot(312)
    sns.barplot(course2_costs, color='green')

    plt.subplot(313)
    sns.barplot(course3_costs, color='red')
    plt.show()


def determine_meal_drinks(data, outfile):
    with open(outfile, 'w') as f:
        writer = csv.writer(f, dialect='excel')
        header = ['CLIENT_ID', 'TIME', 'FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE',
                  'FIRST_COURSE_FOOD', 'FIRST_COURSE_DRINKS', 'SECOND_COURSE_FOOD',
                  'SECOND_COURSE_DRINKS', 'THIRD_COURSE_FOOD', 'THIRD_COURSE_DRINKS']
        writer.writerow(header)
        for id, time, course1, course2, course3 in data:
            course1_food, course2_food, course3_food = '', '', ''
            course1_drinks, course2_drinks, course3_drinks = INF, INF, INF
            for name, price in MENU.items():
                if course1 >= price:
                    if course1 - price < course1_drinks:
                        course1_drinks = course1 - price
                        course1_food = name
                if course2 >= price:
                    if course2 - price < course2_drinks:
                        course2_drinks = course2 - price
                        course2_food = name
                if course3 >= price:
                    if course3 - price < course3_drinks:
                        course3_drinks = course3 - price
                        course3_food = name

            if course1_drinks == INF:
                course1_drinks = 0
            if course2_drinks == INF:
                course2_drinks = 0
            if course3_drinks == INF:
                course3_drinks = 0

            writer.writerow([id, time, course1, course2, course3, course1_food, course1_drinks,
                             course2_food, course2_drinks, course3_food, course3_drinks])

def kmeans(data, outfile):
    model = KMeans(n_clusters=4)
    train_data = np.array([item[2:] for item in data])
    model.fit(train_data)
    predict_labels = model.predict(train_data)
    with open(outfile, 'w') as f:
        writer = csv.writer(f, dialect='excel',lineterminator='\n')
        header = ['CLIENT_ID','TIME', 'FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE', 'LABEL']
        writer.writerow(header)
        for idx, line in enumerate(data):
            line.append(predict_labels[idx])
            writer.writerow(line)

if __name__ == '__main__':
    data = read_data(os.path.join('..\data', 'part1.csv'))
    kmeans(data, 'part1_group.csv')


