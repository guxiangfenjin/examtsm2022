import os
import csv
from matplotlib import pyplot as plt

CLIENTS = {0: 'Onetime', 1: 'Retirement', 2: 'Business', 3: 'Healthy'}
MENU = {'Soup': 'Starters', 'Tomato-Mozarella': 'Starters', 'Oysters': 'Starters',
        'Salad': 'Mains', 'Spaghetti': 'Mains', 'Steak': 'Mains', 'Lobster': 'Mains',
        'Ice cream': 'Desserts', 'Pie': 'Desserts'}


def read_data(infile):
    with open(infile) as f:
        row = csv.reader(f, delimiter=',')
        header = next(row)  # read first line
        data = []
        for r in row:
            data.append(r)
    return data


def plot(likelihoods):
    plt.figure(figsize=(8, 8))
    for idx in range(4):
        plt.subplot(2, 2, idx + 1)
        plt.title(CLIENTS[idx])
        plt.pie(likelihoods[idx], labels=['Starters', 'Mains', 'Desserts'])
    plt.show()


if __name__ == '__main__':
    data = read_data(os.path.join('..', 'part1_food_drinks.csv'))
    group_ids = [int(item[-1]) for item in read_data(os.path.join('..', 'part1_group.csv')) if len(item) != 0]
    counts = {0: [0, 0, 0], 1: [0, 0, 0], 2: [0, 0, 0], 3: [0, 0, 0]}  # group_id: [Starters, Mains, Desserts]
    total_courses = {0: 0, 1: 0, 2: 0, 3: 0}
    idx = 0
    for items in data:
        if len(items) == 0:
            continue
        three_course_food = [items[5], items[7], items[9]]
        for course_food in three_course_food:
            if course_food != '':
                total_courses[group_ids[idx]] += 1
                if MENU[course_food] == 'Starters':
                    counts[group_ids[idx]][0] += 1
                elif MENU[course_food] == 'Mains':
                    counts[group_ids[idx]][1] += 1
                else:
                    counts[group_ids[idx]][2] += 1
        idx += 1

    results = []
    for i in range(4):
        likelihood = []
        for j in range(3):
            likelihood.append(counts[i][j] / float(total_courses[i]))
        results.append(likelihood)
    print(results)
    plot(results)
