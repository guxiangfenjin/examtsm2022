import os
import csv
from matplotlib import pyplot as plt


CLIENTS={0: 'Onetime', 1: 'Retirement', 2: 'Business', 3: 'Healthy'}
COURSES={0: 'First Course', 1: 'Second Course', 2: 'Third Course'}

def read_data(infile):
    with open(infile) as f:
        row = csv.reader(f, delimiter = ',')
        header = next(row)  # read first line
        data = []
        for r in row:
            data.append(r)
    return data

def plot(likelihoods):
    plt.figure(figsize=(18, 8))

    idx = 1
    for j in range(3):
        for i in range(4):
            plt.subplot(3, 4, idx)
            idx += 1
            plt.title(CLIENTS[i] + '/' + COURSES[j])
            plt.pie(likelihoods[i][j].values(), labels=likelihoods[i][j].keys())
    plt.show()

if __name__ == '__main__':
    data = read_data(os.path.join('..', 'part1_food_drinks.csv'))
    group_ids = [int(item[-1]) for item in read_data(os.path.join('..', 'part1_group.csv')) if len(item)!=0]
    counts = {0:[{},{},{}], 1:[{},{},{}], 2:[{},{},{}], 3:[{},{},{}]}
    total_courses = {0:[0,0,0], 1:[0,0,0], 2:[0,0,0], 3:[0,0,0]}
    idx = 0
    for items in data:
        if len(items) == 0:
            continue
        three_course_food = [items[5], items[7], items[9]]
        for course_id, course_food in enumerate(three_course_food):
            if course_food != '':
                group_id = group_ids[idx]
                total_courses[group_id][course_id] += 1
                if course_food not in counts[group_id][course_id]:
                    counts[group_id][course_id][course_food] = 1
                else:
                    counts[group_id][course_id][course_food] += 1
        idx += 1

    results = []
    for i in range(4):
        tmp = []
        for j in range(3):
            probs = {}
            for food, num in counts[i][j].items():
                probs[food] = num / float(total_courses[i][j])
            tmp.append(probs)
        results.append(tmp)
    print(results)
    plot(results)